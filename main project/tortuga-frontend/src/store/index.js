import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";
import SmartTable from 'vuejs-smart-table'

Vue.use(Vuex)
Vue.use(SmartTable)

export default new Vuex.Store({
  state: {
    students: [],
    trainers: [],
    courses: [],
    availableRooms: [],
    dashboardStats: {}
  },
  getters: {
  },
  mutations: {
    LOAD_STUDENTS(state, students) {
      state.students = students;
    },
    SAVE_STUDENT(state, student) {
      state.students.push(student);
    },
    DELETE_STUDENT(state, id) {
      const studentIndex = state.students.findIndex(student => student.id === id)
      state.students.splice(studentIndex,1)
    },
    EDIT_STUDENT(state, student) {
      const studentIndex = state.students.findIndex(_student => _student.id === student.id)
      state.students.splice(studentIndex, 1, student)
    },
    LOAD_TRAINERS(state, trainers) {
      state.trainers = trainers;
    },
    SAVE_TRAINER(state, trainer) {
      state.trainers.push(trainer);
    },
    DELETE_TRAINER(state, id) {
      const trainerIndex = state.trainers.findIndex(trainer => trainer.id === id)
      state.trainers.splice(trainerIndex, 1)
    },
    EDIT_TRAINER(state, trainer) {
      const trainerIndex = state.trainers.findIndex(_trainer => _trainer.id === trainer.id)
      state.trainers.splice(trainerIndex, 1, trainer)
    },
    LOAD_COURSES(state, courses) {
      state.courses = courses
    },
    SAVE_COURSE(state, course) {
      state.courses.push(course);
    },
    DELETE_COURSE(state, id) {
      const courseIndex = state.courses.findIndex(course => course.id === id)
      state.courses.splice(courseIndex, 1)
    },
    LOAD_AVAILABLE_ROOMS(state, rooms) {
      state.availableRooms = rooms;
    },
    LOAD_DASHBOARD_STATS(state, stats) {
      state.dashboardStats = stats;
    },

  },
  actions: {
    async loadStudents(store) {
      const response = await axios.get("api/students")
      store.commit("LOAD_STUDENTS", response.data);
    },
    async saveStudent(store, student) {
      const response = await axios.post("api/student", student)
      store.commit("SAVE_STUDENT", response.data);
    },
    async deleteStudent(store, id) {
      await axios.delete("api/student/" + id)
      store.commit("DELETE_STUDENT", id);
    },
    async editStudent(store, student) {
      await axios.put("api/student/" + student.id, student)
      store.commit("EDIT_STUDENT", student);
    },
    async loadTrainers(store) {
      const response = await axios.get("api/trainers")
      store.commit("LOAD_TRAINERS", response.data);
    },
    async saveTrainer(store, trainer) {
      const response = await axios.post("api/trainer", trainer)
      store.commit("SAVE_TRAINER", response.data);
    },
    async deleteTrainer(store, id) {
      await axios.delete("api/trainer/" + id)
      store.commit("DELETE_TRAINER", id);
    },
    async editTrainer(store, trainer) {
      await axios.put("/api/trainer/" + trainer.id, trainer)
      store.commit("EDIT_TRAINER", trainer)
    },
    async loadCourses(store) {
      const response = await axios.get("api/courses")
      store.commit("LOAD_COURSES", response.data);
    },
    async saveCourse(store, course) {
      const response = await axios.post("api/course", course)
      store.commit("SAVE_COURSE", response.data)
    },
    async deleteCourse(store, id) {
      await axios.delete("api/course/" + id)
      store.commit("DELETE_COURSE", id);
    },
    async loadAvailableRooms(store) {
      const response = await axios.get("api/rooms/available")
      store.commit("LOAD_AVAILABLE_ROOMS", response.data)
    },
    async loadDashboardStats(state) {
      const response = await axios.get("api/dashboard")
      state.commit("LOAD_DASHBOARD_STATS", response.data)

    },

  }
})
