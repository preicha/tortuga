import Vue from 'vue'
import VueRouter from 'vue-router'

import StudentView from "@/views/StudentView.vue"
import DashboardView from "@/views/DashboardView"
import TrainerView from "@/views/TrainerView"
import CourseView from "@/views/CourseView"

Vue.use(VueRouter)

const routes = [
    {
      path: '/',
      name: 'dashboard',
      component: DashboardView
    },
    {
    path: '/students',
    name: 'students',
    component: StudentView
    },
    {
    path: '/trainers',
    name: 'trainers',
    component: TrainerView
    },
    {
    path: '/courses',
    name: 'courses',
    component: CourseView
    }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
