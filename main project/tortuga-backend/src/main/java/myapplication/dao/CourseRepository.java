package myapplication.dao;

import myapplication.model.Course;
import myapplication.model.CourseType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

//set class to repository
@Repository
public interface CourseRepository extends CrudRepository<Course, Integer> {

    //implement method to fetch course by unique id
    Course findById(int id);

    //implement method to fetch a set of students by course type
    Set<Course> findByCourseType(CourseType courseType);

}
