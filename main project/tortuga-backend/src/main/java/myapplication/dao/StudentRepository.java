package myapplication.dao;

import myapplication.model.CourseType;
import myapplication.model.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

//set class to repository
@Repository
public interface StudentRepository extends CrudRepository<Student, Integer> {

    //implement method to fetch student by unique id
    Student findById(int id);

    //implement method to fetch a set of students by course type
    Set<Student> findByCourseType(CourseType courseType);

    Set<Student> findByIsEnrolled(boolean isEnrolled);

}
