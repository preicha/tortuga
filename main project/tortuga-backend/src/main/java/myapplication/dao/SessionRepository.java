package myapplication.dao;

import myapplication.model.Session;
import myapplication.model.Trainer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface SessionRepository extends CrudRepository<Session, Integer> {

    Session findById(int id);

    Set<Session> findByTrainer(Trainer trainer);
}
