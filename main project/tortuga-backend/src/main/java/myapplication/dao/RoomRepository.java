package myapplication.dao;

import myapplication.model.Room;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

//set class to repository
@Repository
public interface RoomRepository extends CrudRepository<Room, Integer> {

    //implement method to fetch room by id
    Room findById(int id);

    //implement method to fetch rooms by occupation status
    List<Room> findByIsOccupied(boolean isOccupied);
}
