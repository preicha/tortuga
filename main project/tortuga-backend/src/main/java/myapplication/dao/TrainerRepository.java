package myapplication.dao;

import myapplication.model.Subject;
import myapplication.model.Trainer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

//set class to repository
@Repository
public interface TrainerRepository extends CrudRepository<Trainer, Integer> {

    //implement method to fetch trainer by unique id
    Trainer findById(int id);

    //implement method to fetch a set of trainers by teaching subject
    Set<Trainer> findByTeachingSubject(Subject teachingSubject);
}
