package myapplication.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
@Table(name = "sessions")
public class Session {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    @ManyToOne
    @JsonBackReference(value = "session-course")
    @JoinColumn(name = "course_id")
    private Course course;

    @ManyToOne
    @JsonBackReference(value = "session-trainer")
    @JoinColumn(name = "trainer_id")
    private Trainer trainer;

    private Subject subject;

    private WorkDay day;

    public Session() {
    }

    public Session(Course course, Trainer trainer, Subject subject, WorkDay day) {
        this.course = course;
        this.trainer = trainer;
        this.subject = subject;
        this.day = day;
    }

    public int getId() {
        return id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public WorkDay getDay() {
        return day;
    }

    public void setDay(WorkDay day) {
        this.day = day;
    }
}
