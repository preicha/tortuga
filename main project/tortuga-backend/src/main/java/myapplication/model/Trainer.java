package myapplication.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

//rename table and have spring boot accept trainer as an entity to create tables
@Entity
@Table(name = "trainers")
public class Trainer extends Person{

    //rename column
    @Column(name = "salary_gross")
    private double salaryGross;

    //rename column
    @Column(name = "salary_net")
    private double salaryNet;

    //set residence for trainer
    private String residence;

    //rename column and set to not nullable
    @Column(name = "teaching_subject", nullable = false)
    //set to type .string if we want to add new enum values or rearrange
    @Enumerated(EnumType.STRING)
    private Subject teachingSubject;

    //rename column
    @Column(name = "available_days")
    //let spring boot know of the list type
    @ElementCollection
    //set to type .string if we want to add new enum values or rearrange
    @Enumerated(EnumType.STRING)
    private List<WorkDay> availableDays;

    //one trainer has many session
    @OneToMany(mappedBy = "trainer")
    //prevent infinity loops
    @JsonBackReference(value = "session-trainer")
    private Set<Session> sessions;

    //default constructor for table generation
    public Trainer() {
    }

    //constructor for adding new entities
    public Trainer(String firstName, String lastName, Gender gender, double salaryGross, String residence, Subject teachingSubject, List<WorkDay> availableDays) {
        super(firstName, lastName, gender);
        this.salaryGross = salaryGross;
        this.residence = residence;
        this.teachingSubject = teachingSubject;
        this.availableDays = availableDays;
        this.salaryNet = calculateSalaryNet();
    }

    //getter & setter
    public double getSalaryGross() {
        return salaryGross;
    }

    public void setSalaryGross(double salaryGross) {
        this.salaryGross = salaryGross;
        this.salaryNet = calculateSalaryNet();
    }

    //implemented net salary calculation based on austrian pay table
    private double calculateSalaryNet() {
        if (salaryGross < 1099.33) {
            salaryNet = salaryGross;
        } else if (salaryGross < 1516.00) {
            salaryNet = salaryGross - (salaryGross * 0.2);
        } else if (salaryGross < 2599.33) {
            salaryNet = salaryGross - (salaryGross * 0.325);
        } else if (salaryGross < 5016.00) {
            salaryNet = salaryGross - (salaryGross * 0.42);
        } else if (salaryGross < 7516.00) {
            salaryNet = salaryGross - (salaryGross * 0.48);
        } else if (salaryGross < 83349.33) {
            salaryNet = salaryGross - (salaryGross * 0.5);
        } else {
            salaryNet = salaryGross - (salaryGross * 0.55);
        }
        return salaryNet;
    }

    public double getSalaryNet() { return salaryNet; }

    public void setSalaryNet(double salaryNet) {
        this.salaryNet = salaryNet;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public Subject getTeachingSubject() {
        return teachingSubject;
    }

    public void setTeachingSubject(Subject teachingSubject) {
        this.teachingSubject = teachingSubject;
    }

    public List<WorkDay> getAvailableDays() {
        return availableDays;
    }

    public void setAvailableDays(List<WorkDay> availableDays) {
        this.availableDays = availableDays;
    }

    public Set<Session> getSessions() {
        return sessions;
    }

    public void setSessions(Set<Session> sessions) {
        this.sessions = sessions;
    }
}
