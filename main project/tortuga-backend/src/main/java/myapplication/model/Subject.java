package myapplication.model;

public enum Subject {
    LINUX("Linux"),
    WINDOWS("Windows"),
    WEB("Web"),
    JAVA("Java"),
    DATABASES("Databases");

    //setup displayValue for easier readability on front end
    private final String displayValue;

    //getter & setter
    private Subject(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
