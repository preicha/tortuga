package myapplication.model;

import javax.persistence.*;

//have spring boot accept person as an entity to create tables
@Entity
//let each child object get their own table
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Person {
    //have int id set to primary key
    @Id
    //set it to auto_increment
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    //rename column and set to not nullable
    @Column(name = "first_name", nullable = false)
    private String firstName;

    //rename column and set to not nullable
    @Column(name = "last_name", nullable = false)
    private String lastName;

    //set column to not nullable
    @Column(nullable = false)
    //set to type .string if we want to add new enum values or rearrange
    @Enumerated(EnumType.STRING)
    private Gender gender;

    //default constructor for table generation
    public Person() {}

    //constructor for adding new entities
    public Person(String firstName, String lastName, Gender gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
    }

    //getter & setter
    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }
}
