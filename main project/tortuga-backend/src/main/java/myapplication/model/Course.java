package myapplication.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

//have spring boot accept course as an entity to create tables
@Entity
@Table(name = "courses")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //basic naming (will be upgraded to COURSE_TYPE/Number)
    @Column(name = "course_name")
    private String courseName;

    @Column(name = "course_type")
    @Enumerated(EnumType.STRING)
    private CourseType courseType;

    private int number;

    //one course has many students
    @OneToMany(mappedBy = "course")
    //prevent infinity loops
    @JsonManagedReference
    private Set<Student> students = new HashSet<>();

    //one course has one room
    @OneToOne
    //foreign key added to course table with room_id
    @JoinColumn(name = "room_id")
    //prevent infinity loops
    @JsonManagedReference
    private Room room;

    //one course has many sessions
    @OneToMany(mappedBy = "course")
    //prevent infinity loops
    @JsonManagedReference(value = "session-course")
    private Set<Session> sessions = new HashSet<>();

    //default constructor for table generation
    public Course() {
    }

    //constructor for adding new entities
    public Course(CourseType courseType, int number, Room room) {
        this.courseType = courseType;
        this.number = number;
        this.room = room;
        this.courseName = courseType.getDisplayValue() + " " + String.format("%02d", number);
    }

    //getter & setter
    public int getId() {
        return id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Student student) {
        this.students.add(student);
    }

    public CourseType getCourseType() {
        return courseType;
    }

    public void setCourseType(CourseType courseType) {
        this.courseType = courseType;
        this.courseName = courseType.getDisplayValue() + " " + String.format("%02d", number);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
        this.courseName = courseType.getDisplayValue() + " " + String.format("%02d", number);
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Set<Session> getSessions() {
        return sessions;
    }

    public void setSessions(Set<Session> sessions) {
        this.sessions = sessions;
    }
}
