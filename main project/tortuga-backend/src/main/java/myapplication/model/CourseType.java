package myapplication.model;

import java.util.HashSet;
import java.util.Set;

public enum CourseType {
    SOFTWARE_DEVELOPMENT("SW"),
    NETWORKING("NT"),
    FIA("FIA"),
    Q_SOFTWARE_DEVELOPMENT("Q SW"),
    Q_NETWORKING("Q NT"),
    Q_FIA("Q FIA");

    private final String displayValue;

    private CourseType(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public static Set<Subject> getValidSubjects(CourseType courseType) {
        Set<Subject> validSubjects = new HashSet<>();
        switch (courseType) {
            case SOFTWARE_DEVELOPMENT:
            case Q_SOFTWARE_DEVELOPMENT:
                validSubjects.add(Subject.WEB);
                validSubjects.add(Subject.JAVA);
                validSubjects.add(Subject.DATABASES);
                break;
            case NETWORKING:
            case Q_NETWORKING:
                validSubjects.add(Subject.LINUX);
                validSubjects.add(Subject.WINDOWS);
                break;
            case FIA:
            case Q_FIA:
                validSubjects.add(Subject.WEB);
                validSubjects.add(Subject.JAVA);
                validSubjects.add(Subject.DATABASES);
                validSubjects.add(Subject.LINUX);
                validSubjects.add(Subject.WINDOWS);
                break;
            default:
                break;
        }
        return validSubjects;
    }
}
