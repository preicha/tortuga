package myapplication.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

//have spring boot accept room as an entity to create tables
@Entity
@Table(name = "rooms")
public class Room {
    //have int id set to primary key
    @Id
    //set it to auto_increment
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int number;
    private String name;

    @Column(name = "occupied")
    private boolean isOccupied;

    //one room has one course and relationship is handled by table room
    @OneToOne(mappedBy = "room")
    //prevent infinity loops
    @JsonBackReference
    private Course course;


    //default constructor for table generation
    public Room() {
    }

    //constructor for adding new entities
    public Room(int number, String name, boolean isOccupied) {
        this.number = number;
        this.name = name;
        this.isOccupied = isOccupied;
    }

    //getter & setter
    public int getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOccupied() {
        return isOccupied;
    }

    public void setOccupied(boolean occupied) {
        isOccupied = occupied;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", number=" + number +
                ", name='" + name + '\'' +
                ", isOccupied=" + isOccupied +
                '}';
    }
}
