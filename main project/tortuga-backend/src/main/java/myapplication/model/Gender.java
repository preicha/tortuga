package myapplication.model;

public enum Gender {
    MALE("Male"),
    FEMALE("Female"),
    DIVERS("Divers");

    //setup displayValue for easier readability on front end
    private final String displayValue;

    //getter & setter
    private Gender(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
