package myapplication.model;

public enum WorkDay {
    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday");

    //setup displayValue for easier readability on front end
    private final String displayValue;

    //getter & setter
    private WorkDay(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
