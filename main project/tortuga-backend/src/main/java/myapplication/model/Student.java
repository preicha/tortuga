package myapplication.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;

//rename table and have spring boot accept student as an entity to create tables
@Entity
@Table(name = "students")
public class Student extends Person{
    //rename column
    @Column(name = "enrolled")
    private boolean isEnrolled;

    //define a course type for the student when generating the student
    @Column(name = "course_type")
    private CourseType courseType;

    //set a start date for the student
    @Column(name = "start_date")
    private Date startDate;

    //many students are in a course
    @ManyToOne
    //prevent infinity loops
    @JsonBackReference
    //rename column of foreign key
    @JoinColumn(name = "course_id")
    private Course course;

    //default constructor for table generation
    public Student() {
    }

    //constructor for adding new entities
    public Student(String firstName, String lastName, Gender gender, CourseType courseType, Date startDate) {
        super(firstName, lastName, gender);
        this.courseType = courseType;
        this.startDate = startDate;
    }

    //getter & setter
    public boolean isEnrolled() {
        return isEnrolled;
    }

    public void setEnrolled(boolean enrolled) {
        isEnrolled = enrolled;
    }

    public CourseType getCourseType() {
        return courseType;
    }

    public void setCourseType(CourseType courseType) {
        this.courseType = courseType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id= " + getId() +
                "isEnrolled=" + isEnrolled +
                ", courseType=" + courseType +
                ", course=" + course +
                '}';
    }
}
