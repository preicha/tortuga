package myapplication.dtos;

import java.util.Set;

public class StudentDTO {
    private Set<Integer> studentIds;

    //getter & setter
    public Set<Integer> getStudentIds() {
        return studentIds;
    }

    public void setStudentIds(Set<Integer> studentIds) {
        this.studentIds = studentIds;
    }
}
