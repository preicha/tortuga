package myapplication.dtos;

import myapplication.model.Subject;
import myapplication.model.WorkDay;

public class TrainerScheduleDTO {

    private int sessionId;
    private String courseName;
    private String trainerName;
    private Subject subject;
    private WorkDay day;
    private String roomName;


    //getter & setter
    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTrainerName() {
        return trainerName;
    }

    public void setTrainerName(String trainerName) {
        this.trainerName = trainerName;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public WorkDay getDay() {
        return day;
    }

    public void setDay(WorkDay day) {
        this.day = day;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
