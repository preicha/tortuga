package myapplication.dtos;

import myapplication.model.Course;
import myapplication.model.Subject;
import myapplication.model.Trainer;
import myapplication.model.WorkDay;

public class SessionDTO {
    private int courseID;
    private int trainerID;
    private Subject subject;
    private WorkDay day;

    //getter & setter
    public int getCourseID() {
        return courseID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }

    public int getTrainerID() {
        return trainerID;
    }

    public void setTrainerID(int trainerID) {
        this.trainerID = trainerID;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public WorkDay getDay() {
        return day;
    }

    public void setDay(WorkDay day) {
        this.day = day;
    }
}
