package myapplication.dtos;

import myapplication.model.Gender;
import myapplication.model.Subject;
import myapplication.model.WorkDay;

import java.util.List;

public class TrainerDTO {

    private String firstName;
    private String lastName;
    private Gender gender;
    private double salaryGross;
    private String residence;
    private Subject teachingSubject;
    private List<WorkDay> availableDays;

    //might want to change back to List and fill List via frontend input
    private boolean isSelfEmployed;

    public TrainerDTO(String firstName, String lastName, Gender gender, double salaryGross, String residence, Subject teachingSubject, List<WorkDay> availableDays, boolean isSelfEmployed) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.salaryGross = salaryGross;
        this.residence = residence;
        this.teachingSubject = teachingSubject;
        this.availableDays = availableDays;
        this.isSelfEmployed = isSelfEmployed;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public double getSalaryGross() {
        return salaryGross;
    }

    public void setSalaryGross(double salaryGross) {
        this.salaryGross = salaryGross;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public Subject getTeachingSubject() {
        return teachingSubject;
    }

    public void setTeachingSubject(Subject teachingSubject) {
        this.teachingSubject = teachingSubject;
    }

    public List<WorkDay> getAvailableDays() {
        return availableDays;
    }

    public void setAvailableDays(List<WorkDay> availableDays) {
        this.availableDays = availableDays;
    }

    public boolean isSelfEmployed() {
        return isSelfEmployed;
    }

    public void setSelfEmployed(boolean selfEmployed) {
        isSelfEmployed = selfEmployed;
    }
}
