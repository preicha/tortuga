package myapplication.service;

import myapplication.dao.TrainerRepository;
import myapplication.model.Trainer;
import myapplication.dtos.TrainerDTO;
import myapplication.model.WorkDay;
import myapplication.utils.RecordNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


//have service class handle the business layer
@Service
public class TrainerService {

    //dependency injection of trainer repository
    @Autowired
    TrainerRepository trainerRepository;


    //get all trainers
    public List<Trainer> getTrainers() {
        List<Trainer> allTrainers = new ArrayList<>();
        trainerRepository.findAll().forEach(trainer -> allTrainers.add(trainer));
        return allTrainers;
    }

    //post trainer
    public Trainer postTrainer(TrainerDTO trainerDTO) {
        List<WorkDay> availableDays = new ArrayList<>();
        boolean trainerSelfEmployed = trainerDTO.isSelfEmployed();
        //if trainer is not self-employed set available days equal to each workday
        if (!trainerSelfEmployed) {
            availableDays = Arrays.asList(WorkDay.values());
        } else {
            //else set available days to <available days>
            availableDays = trainerDTO.getAvailableDays();
        }
        Trainer newTrainer = trainerRepository.save(new Trainer(trainerDTO.getFirstName(), trainerDTO.getLastName(),
                trainerDTO.getGender(), trainerDTO.getSalaryGross(), trainerDTO.getResidence(), trainerDTO.getTeachingSubject(), availableDays));
        return newTrainer;
    }

    //edit trainer
    public void putTrainer(int id, Trainer trainer) throws Exception{
        Trainer trainerToEdit = trainerRepository.findById(id);
        if (trainerToEdit == null) {
            throw new RecordNotFoundException(id, "putTrainer");
        }
        trainerToEdit.setFirstName(trainer.getFirstName());
        trainerToEdit.setLastName(trainer.getLastName());
        trainerToEdit.setGender(trainer.getGender());
        trainerToEdit.setSalaryGross(trainer.getSalaryGross());
        trainerToEdit.setTeachingSubject(trainer.getTeachingSubject());
        trainerToEdit.setAvailableDays(trainer.getAvailableDays());
        Trainer trainerEdited = trainerRepository.save(trainerToEdit);
    }


}
