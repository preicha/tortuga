package myapplication.service;

import myapplication.dao.StudentRepository;
import myapplication.model.Student;
import myapplication.utils.RecordNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    public void putStudent(int id, Student student) throws Exception {
        //check if student with path id exists
        Student studentToEdit = studentRepository.findById(id);
        if (studentToEdit == null) {
            //console output for not found
            throw new RecordNotFoundException(id, "putStudent");
        }
        studentToEdit.setFirstName(student.getFirstName());
        studentToEdit.setLastName(student.getLastName());
        studentToEdit.setGender(student.getGender());
        Student studentEdited = studentRepository.save(studentToEdit);
    }
}
