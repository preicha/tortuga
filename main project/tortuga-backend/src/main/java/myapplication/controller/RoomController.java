package myapplication.controller;

import myapplication.dao.RoomRepository;
import myapplication.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/")
public class RoomController {

    //dep inj of room repository
    @Autowired
    RoomRepository roomRepository;

    @GetMapping("rooms")
    public List<Room> getRooms() {
        List allRooms = new ArrayList<>();
        roomRepository.findAll().forEach(room -> allRooms.add(room));
        return allRooms;
    }

    @GetMapping("rooms/available")
    public List<Room> getAvailableRooms() {
        List availableRooms = new ArrayList<>();
        roomRepository.findByIsOccupied(false).forEach(room -> availableRooms.add(room));
        return availableRooms;
    }
}
