package myapplication.controller;

import myapplication.dao.CourseRepository;
import myapplication.dao.SessionRepository;
import myapplication.dao.TrainerRepository;
import myapplication.dtos.SessionDTO;
import myapplication.model.Course;
import myapplication.model.Session;
import myapplication.model.Trainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/")
public class SessionController {

    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    TrainerRepository trainerRepository;

    @GetMapping("sessions")
    public List<Session> getSessions() {
        List<Session> allSessions = new ArrayList<>();
        sessionRepository.findAll().forEach(session -> allSessions.add(session));
        return allSessions;
    }

    @PostMapping("session")
    public void setSession(@RequestBody SessionDTO sessionDTO) {
        Course myCourse = courseRepository.findById(sessionDTO.getCourseID());
        Trainer myTrainer = trainerRepository.findById(sessionDTO.getTrainerID());
        sessionRepository.save(new Session(myCourse, myTrainer, sessionDTO.getSubject(), sessionDTO.getDay()));
    }
}
