package myapplication.controller;

import myapplication.dao.StudentRepository;
import myapplication.dao.TrainerRepository;
import myapplication.dtos.DashboardStudentTrainerCountDTO;
import myapplication.model.Student;
import myapplication.model.Trainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/")
public class DashboardController {

    //dependency injection of trainer repository (will be removed after full implementation of service class)
    @Autowired
    TrainerRepository trainerRepository;

    //dependency injection of student repository (will be removed after full implementation of service class)
    @Autowired
    StudentRepository studentRepository;

    @GetMapping("dashboard")
    public DashboardStudentTrainerCountDTO getStatsForDashboard() {
        DashboardStudentTrainerCountDTO studentTrainerStats = new DashboardStudentTrainerCountDTO();
        List<Trainer> allTrainers = (List<Trainer>) trainerRepository.findAll();
        List<Student> allStudents = (List<Student>) studentRepository.findAll();
        studentTrainerStats.setTrainerCount(allTrainers.size());
        studentTrainerStats.setStudentCount(allStudents.size());
        return studentTrainerStats;
    }


}
