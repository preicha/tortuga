package myapplication.controller;

import myapplication.dao.CourseRepository;
import myapplication.dao.SessionRepository;
import myapplication.dao.TrainerRepository;
import myapplication.dtos.TrainerScheduleDTO;
import myapplication.model.Session;
import myapplication.model.Subject;
import myapplication.model.Trainer;
import myapplication.dtos.TrainerDTO;
import myapplication.model.WorkDay;
import myapplication.service.TrainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/")
public class TrainerController {

    //dependency injection of trainer repository (will be removed after full implementation of trainer service class)
    @Autowired
    TrainerRepository trainerRepository;

    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    CourseRepository courseRepository;

    //dependency injection of trainer service
    @Autowired
    TrainerService trainerService;

    //accept a get request from front end and return a list of all trainers
    @GetMapping("trainers")
    public List<Trainer> getTrainers() {
        return trainerService.getTrainers();
    }

    //accept a get request from front end and return a list of all available days for a trainer
    @GetMapping("trainers/{id}")
    //identify trainer to check via id
    public List<WorkDay> getAvailableDaysFromTrainer(@PathVariable int id) {
        Trainer trainer = trainerRepository.findById(id);
        List<WorkDay> availableDays = trainer.getAvailableDays();
        return availableDays;
    }

    //accept a get request from front end and return a list of all trainers with a given teaching subject
    @GetMapping("trainers/subject/{teachingSubject}")
    public Set<Trainer> getAvailableTrainers(@PathVariable Subject teachingSubject) {
        return trainerRepository.findByTeachingSubject(teachingSubject);
    }

    @GetMapping("trainer/schedule/{id}")
    public Set<TrainerScheduleDTO> getSchedule(@PathVariable int id) {
        Set<TrainerScheduleDTO> schedules = new HashSet<>();
        Trainer myTrainer = trainerRepository.findById(id);
        sessionRepository.findByTrainer(myTrainer).forEach(session -> {
            TrainerScheduleDTO entry = new TrainerScheduleDTO();
            entry.setSessionId(session.getId());
            entry.setCourseName(session.getCourse().getCourseName());
            entry.setTrainerName(session.getTrainer().getFirstName() + " " + session.getTrainer().getLastName());
            entry.setDay(session.getDay());
            entry.setSubject(session.getSubject());
            entry.setRoomName(session.getCourse().getRoom().getName());
            schedules.add(entry);
        });
        return schedules;
    }

    //accept post request from frontend with DTO (setting boolean not working)
    @PostMapping("trainer")
    public Trainer postTrainer(@RequestBody TrainerDTO trainerDTO) {
        return trainerService.postTrainer(trainerDTO);
    }

    //accept put request from front end and update existing trainer
    @PutMapping("trainer/{id}")
    //identify trainer to edit via id and update properties according to body content (might want to add DTO for parameter)
    public void putTrainer(@PathVariable int id, @RequestBody Trainer trainer) throws Exception {
        trainerService.putTrainer(id, trainer);
    }

    //accept delete request from front end and delete all trainers
    @DeleteMapping("trainers")
    public void deleteAllTrainers() {
        trainerRepository.deleteAll();
    }

    //accept delete request from front end and delete a single trainer
    @DeleteMapping("trainer/{id}")
    //identify trainer to delete via id
    public void deleteTrainer(@PathVariable int id) {
        trainerRepository.deleteById(id);
    }
}
