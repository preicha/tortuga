package myapplication.controller;

import myapplication.dao.CourseRepository;
import myapplication.dao.RoomRepository;
import myapplication.dao.StudentRepository;
import myapplication.dtos.StudentDTO;
import myapplication.dtos.TrainerDTO;
import myapplication.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

@RestController
@RequestMapping("api/")
public class CourseController {

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    RoomRepository roomRepository;

    //accept a get request from front end and return a list of all courses
    @GetMapping("courses")
    public List<Course> getCourses() {
        List<Course> allCourses = new ArrayList<>();
        courseRepository.findAll().forEach(course -> allCourses.add(course));
        return allCourses;
    }

    //accept a get request from front end and return a list of all students with the same course type as the course with the id
    @GetMapping("course/{id}")
    public Set<Student> getValidStudents(@PathVariable int id) {
        Course myCourse = courseRepository.findById(id);
        CourseType courseTypeToCheck = myCourse.getCourseType();
        Set<Student> studentsWithCourse = studentRepository.findByCourseType(courseTypeToCheck);
        return studentsWithCourse;
    }

    @GetMapping("course/subjects/{id}")
    public Set<Subject> getValidSubjects(@PathVariable int id) {
        Course myCourse = courseRepository.findById(id);
        return CourseType.getValidSubjects(myCourse.getCourseType());
    }

    //standard post request
    @PostMapping("course")
    public Course postCourse(@RequestBody Course course) {
        Room roomToSetOccupationStatus = course.getRoom();
        System.out.println(roomToSetOccupationStatus.toString());
        roomToSetOccupationStatus.setOccupied(true);
//        save room to room repository before setting course but after setting occ status to true
        System.out.println(roomToSetOccupationStatus.toString());
        Course newCourse = courseRepository.save(new Course(course.getCourseType(), course.getNumber(), roomToSetOccupationStatus));
        return newCourse;
    }

    //accept a post request from front end to save students to the course
    @PostMapping("course/{id}")
    public void postStudentsToCourse(@PathVariable int id, @RequestBody StudentDTO students) {
        Course courseToFill = courseRepository.findById(id);
        students.getStudentIds().forEach(studentId -> {
            Student studentToAddToCourse = studentRepository.findById(studentId).get();
            studentToAddToCourse.setCourse(courseToFill);
            studentToAddToCourse.setEnrolled(true);
            courseToFill.setStudents(studentToAddToCourse);
            studentRepository.save(studentToAddToCourse);
            courseRepository.save(courseToFill);
        });
    }

    @DeleteMapping("courses")
    public void deleteAllCourses() {
        courseRepository.deleteAll();
    }

    @DeleteMapping("course/{id}")
    public void deleteCourse(@PathVariable int id) {
        courseRepository.deleteById(id);
    }

}
