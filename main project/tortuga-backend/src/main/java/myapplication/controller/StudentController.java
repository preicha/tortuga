package myapplication.controller;

import myapplication.dao.StudentRepository;
import myapplication.model.Student;
import myapplication.service.StudentService;
import myapplication.utils.AppLogger;
import myapplication.utils.RecordNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/")
public class StudentController {
    //dependency injection of logger
    @Autowired
    //set to consoleLogger for testing, databaseLogger for production
    @Qualifier("consoleLogger")
    AppLogger applogger;

    //dependency injection of student repository
    @Autowired
    StudentRepository studentRepository;

    //dependency injection of student service
    @Autowired
    StudentService studentService;

    //accept a get request from front end and return a list of all students
    @GetMapping("students")
    public List<Student> getStudents() {
        List<Student> allStudents = new ArrayList<>();
        studentRepository.findAll().forEach(student -> allStudents.add(student));
        return allStudents;
    }

    @GetMapping("student/{id}")
    public Student getStudent(@PathVariable int id) {
        return studentRepository.findById(id);
    }

    //accept a post request from frontend to save new entity to table and return new entity
    @PostMapping("student")
    //get entity properties from body content
    public Student postStudent(@RequestBody Student student) {
        Student newStudent = studentRepository.save(new Student(student.getFirstName(), student.getLastName(),
                student.getGender(), student.getCourseType(), student.getStartDate()));
        return newStudent;
    }

    //accept put request from front end and update existing student
    @PutMapping("student/{id}")
    //identify student to edit via id and update properties according to body content (might want to add DTO for parameter)
    public void putStudent(@PathVariable int id, @RequestBody Student student) throws Exception {
        studentService.putStudent(id, student);
    }

    //accept delete request from front end and delete all students
    @DeleteMapping("students")
    public void deleteAllStudents() {
        studentRepository.deleteAll();
    }

    //accept delete request from front end and delete a single student
    @DeleteMapping("student/{id}")
    //identify student to delete via id
    public void deleteStudent(@PathVariable int id) {
        studentRepository.deleteById(id);
    }
}
