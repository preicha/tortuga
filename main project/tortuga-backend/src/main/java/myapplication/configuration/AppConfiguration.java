package myapplication.configuration;

import myapplication.dao.RoomRepository;
import myapplication.dao.StudentRepository;
import myapplication.model.CourseType;
import myapplication.model.Gender;
import myapplication.model.Room;
import myapplication.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

@Configuration
public class AppConfiguration {
    @Autowired
    RoomRepository roomRepository;

    @Autowired
    StudentRepository studentRepository;

    @Bean
    public CommandLineRunner configuration() {
        return args -> {
            //fill database with rooms
            roomRepository.save(new Room(1, "Konrad Zuse", false));
            roomRepository.save(new Room(2, "Grace Hopper", false));
            roomRepository.save(new Room(3, "Alan Turing", false));
            roomRepository.save(new Room(4, "Radia Perlman", false));
            roomRepository.save(new Room(5, "Linus Torvalds", false));
            roomRepository.save(new Room(6, "Ada Lovelace", false));
            roomRepository.save(new Room(7, "James Gosling", false));
            roomRepository.save(new Room(8, "Margaret Hamilton", false));

            //create i random students
            for (int i = 0; i < 50; i++) {
                String firstName = "";
                String lastName = "";
                Gender gender = null;
                CourseType courseType = null;
                Date startDate = null;

                int randomFirstName = (int) (Math.random() * 10);
                int randomLastName = (int) (Math.random() * 10);
                int randomGender = (int) (Math.random() * 3);
                int randomCourseType = (int) (Math.random() * 6);

                //set random firstName
                String[] firstNames = new String[]{"Peter", "Hans", "Ulrich", "Lea", "Angelika", "Anna-Maria", "Tim",
                        "Richard", "Margarethe", "Marvin"};
                firstName = firstNames[randomFirstName];

                //set random lastName
                String[] lastNames = new String[]{"Eberhardt", "Berg", "Luft", "Loewe", "Schweitzer", "Wagner",
                        "Dresdner", "Mauer", "Gottschalk", "Eggers"};
                lastName = lastNames[randomLastName];

                //set random gender
                switch (randomGender) {
                    case 0:
                        gender = Gender.MALE;
                        break;
                    case 1:
                        gender = Gender.FEMALE;
                        break;
                    case 2:
                        gender = Gender.DIVERS;
                        break;
                }

                //set random course type
                switch (randomCourseType) {
                    case 0:
                        courseType = CourseType.SOFTWARE_DEVELOPMENT;
                        break;
                    case 1:
                        courseType = CourseType.NETWORKING;
                        break;
                    case 2:
                        courseType = CourseType.FIA;
                        break;
                    case 3:
                        courseType = CourseType.Q_SOFTWARE_DEVELOPMENT;
                        break;
                    case 4:
                        courseType = CourseType.Q_NETWORKING;
                        break;
                    case 5:
                        courseType = CourseType.Q_FIA;
                        break;
                }

                //add randomly generated students to student database
                try {
                    studentRepository.save(new Student(firstName, lastName, gender, courseType, startDate));
                } catch (Exception e) {
                    System.err.println("Couldn't add Student: " + e.getMessage());
                }
            }
        };
    }

}
