package myapplication.utils;

//create own logger interface
public interface AppLogger {
    void log(String message);
}
