package myapplication.utils;

import org.springframework.stereotype.Component;

//logging in console for testing
@Component("consoleLogger")
public class ConsoleLogger implements AppLogger{
    @Override
    public void log(String message) {
        System.out.println(message);
    }
}
