package myapplication.utils;

//create own class for handling exception for when entity not found
public class RecordNotFoundException extends Exception {
    private int id;
    private String context = "";

    //error message WITHOUT context
    public RecordNotFoundException(int id) {
        this.id = id;
    }

    //error message WITH context
    public RecordNotFoundException(int id, String context) {
        this.id = id;
        this.context = context;
    }

    //override default getMessage() method
    @Override
    public String getMessage() {
        return "Context: " + (context != null ? context : "<unspecified>") + "\nRecord with id " + id + " not found!";
    }

    //getter
    public int getId() {
        return id;
    }
}
