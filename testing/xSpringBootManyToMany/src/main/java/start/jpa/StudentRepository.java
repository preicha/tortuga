package start.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import start.jpa.student.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
