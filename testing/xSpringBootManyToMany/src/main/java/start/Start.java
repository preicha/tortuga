package start;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import start.jpa.SkillRepository;
import start.jpa.StudentRepository;
import start.jpa.skill.Skill;
import start.jpa.student.Student;

@SpringBootApplication
public class Start {
    public static void main(String[] args) {
        SpringApplication.run(Start.class);
    }

    @Bean
    public CommandLineRunner test(SkillRepository skillRepository, StudentRepository studentRepository) {
        return args -> {
            Student student = new Student();
            student.setStudentName("Patric");
            student.setQualification("BE");
            student.setRollNo(5);

            Skill skill = new Skill();
            skill.setSkillName("Java");
            skill.setSkillScore("90%");

            Skill skill2 = new Skill();
            skill.setSkillName("Python");
            skill.setSkillScore("45%");

            student.getSkills().add(skill);
            student.getSkills().add(skill2);

            skill.getStudents().add(student);
            skill2.getStudents().add(student);

            studentRepository.save(student);

            Skill skillJava = skillRepository.findBySkillName("Java");
            System.out.println("List: " + skillJava);
            System.out.println("all students: " + studentRepository.findAll());
        };
    }
}
