package start.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class TrainerController {

    @Autowired
    TrainerRepository trainerRepository;

    @GetMapping("/addTrainer")
    public String trainerForm(Model trainerModel) {
        trainerModel.addAttribute("trainerAttribute", new Trainer());
        trainerModel.addAttribute("trainers", trainerRepository.findAll());
        return "trainerAddPage";
    }

    @PostMapping("/addTrainer")
    public String trainerSubmit(@ModelAttribute Trainer trainer, Model trainerModel) {
        trainerModel.addAttribute("trainerNewAttribute", trainer);
        trainerRepository.save(trainer);
        trainerRepository.findAll().forEach(trainerFound -> System.out.println(trainerFound.toString()));
        return "trainerAddedPage";
    }
}
