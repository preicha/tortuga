package start.database;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "trainer")
public class Trainer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String firstName;

    private String lastName;

    private double salary;

    private Subjects teachingSubject;

    @Column
    @ElementCollection
    @Enumerated(EnumType.STRING)
    private List<WorkDays> availableDays;


    public Trainer() {
    }

    public Trainer(String firstName, String lastName, double salary, Subjects teachingSubject, List<WorkDays> availableDays) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
        this.teachingSubject = teachingSubject;
        this.availableDays = availableDays;
    }

    @Override
    public String toString() {
        return id + ". Trainer: " + firstName + " " + lastName + ". Net salary: " + getSalary() + " EUR. Teaching: " + teachingSubject + ". Available: " + availableDays;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getSalary() {
        return calcSalary(salary);
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Subjects getTeachingSubject() {
        return teachingSubject;
    }

    public void setTeachingSubject(Subjects teachingSubject) {
        this.teachingSubject = teachingSubject;
    }

    public List<WorkDays> getAvailableDays() {
        return availableDays;
    }

    public void setAvailableDays(List<WorkDays> availableDays) {
        this.availableDays = availableDays;
    }

    private double calcSalary(double salary) {
        double newSalary;
        if (salary < 1099.33) {
            newSalary = salary;
        } else if (salary < 1516.00) {
            newSalary = salary - (salary * 0.2);
        } else if (salary < 2599.33) {
            newSalary = salary - (salary * 0.325);
        } else if (salary < 5016.00) {
            newSalary = salary - (salary * 0.42);
        } else if (salary < 7516.00) {
            newSalary = salary - (salary * 0.48);
        } else if (salary < 83349.33) {
            newSalary = salary - (salary * 0.5);
        } else {
            newSalary = salary - (salary * 0.55);
        }
        return (Math.round(newSalary * 100) / 100);
    }
}
