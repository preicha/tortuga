package start.database;

public enum Subjects {
    LINUX("Linux"),
    WINDOWS("Windows"),
    WEB("Web"),
    JAVA("Java"),
    DATABASES("Databases"),
    CAREER_COACHING("Career Coaching");

    private final String displayValue;

    private Subjects(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }


}
