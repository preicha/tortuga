package start.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import start.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
}
