CREATE TABLE expense (
  id int(11) NOT NULL AUTO_INCREMENT,
  item varchar(45) NOT NULL,
  amount float NOT NULL,
  PRIMARY KEY (id)
);

---------------------------------
create database testdb;
drop   database testdb;
delete from expense;