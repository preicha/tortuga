package start;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import start.entity.Expense;
import start.entity.ExpenseRepository;

import java.util.List;

@SpringBootApplication
public class Start implements CommandLineRunner {

    @Autowired
    ExpenseRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(Start.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        System.out.println("findAll");
        Iterable<Expense> all = repository.findAll();
        all.forEach(item -> System.out.println(item.toString()));

        System.out.println();
        System.out.println("myFindAll_");
        List<Expense> expenses = repository.myFindAll_();
        expenses.forEach(item -> System.out.println(item.toString()));

        System.out.println();
        System.out.println("myFindItem");
        List<Expense> breakfast5 = repository.myFindItem1("breakfast", 5);
        breakfast5.forEach(breakfast -> System.out.println(breakfast.toString()));

        System.out.println();
        System.out.println("listWithItemsWithPriceOver");
        List<Expense> expensiveItems = repository.listItemsWithPriceOver(200f);
        expensiveItems.forEach(item -> System.out.println(item.toString()));
    }
}

