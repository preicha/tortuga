package start.entity;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ExpenseRepository extends CrudRepository<Expense, Long> {

    public List<Expense> findByItem(String item);

    @Query("SELECT e FROM Expense e")
    List<Expense> myFindAll_();

    @Query("SELECT e FROM Expense e WHERE e.amount >= :amount_")
    List<Expense> listItemsWithPriceOver(@Param("amount_") float amount);

    @Query("SELECT c FROM Expense c WHERE c.item = :item_ and c.amount = :amount_")
    List<Expense> myFindItem1(@Param("item_") String item, @Param("amount_") float amount);
}


