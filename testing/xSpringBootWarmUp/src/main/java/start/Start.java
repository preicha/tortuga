package start;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import start.sample.Product;
import start.sample.ProductService;

import java.util.List;

@SpringBootApplication
public class Start {
    public static void main(String[] args) {
        SpringApplication.run(Start.class, args);
    }

    @Bean
    public CommandLineRunner demo(ProductService productService) {
        return args -> {
            Product product1 = new Product("testName", "testBrand", "testCountry", 20);
            Product product2 = new Product("testName2", "testBrand2", "testCountry2", 22);
            Product product3 = new Product("testName3", "testBrand3", "testCountry3", 33);
            Product product4 = new Product("testName4", "testBrand4", "testCountry4", 44);
            Product product5 = new Product("testName5", "testBrand5", "testCountry5", 55);

            productService.save(product1);
            productService.save(product2);
            productService.save(product3);
            productService.save(product4);
            productService.save(product5);

            productService.listAll().forEach(product -> System.out.println(product.toString()));
        };
    }
}

//http://localhost:8080/h2-console

