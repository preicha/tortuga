package start.database;

public enum CourseType {
    QUALIFYING("Qualifying"),
    SOFTWARE_DEVELOPING("Software Developer"),
    NETWORK_ADMINISTRATION("Network Administration");

    private final String displayValue;

    private CourseType(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
