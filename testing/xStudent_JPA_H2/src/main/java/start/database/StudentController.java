package start.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class StudentController {

    @Autowired
    StudentRepository studentRepository;

    @GetMapping("/addStudent")
    public String studentForm(Model studentModel) {
        studentModel.addAttribute("studentAttribute", new Student());
        studentModel.addAttribute("students", studentRepository.findAll());
        return "studentAddPage";
    }

    @PostMapping("/addStudent")
    public String studentSubmit(@ModelAttribute Student student, Model studentModel) {
        studentModel.addAttribute("studentNewAttribute", student);
        studentRepository.save(student);
        studentRepository.findAll().forEach(studentFound -> System.out.println(studentFound));
        return "studentAddedPage";
    }
}
