# tortuga



## Description

tortuga is an administrative school management tool developed solely by me as part of my software development course.


## Changelog

02-05-2022                    project made public
29-04-2022                    presentation held
26-03-2022                    report for development course finalized
29-03-2022                    testing folder created
                              adding students to h2 database via webform created in testing
07-04-2022				connected frontend to backend
					adding students to database working
